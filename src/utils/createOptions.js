//create select options function
export function createOptions(data, label){
    let options= [];
    for(let i = 1; i <= Object.keys(data).length; i++){
        //adding a label for the select option
        let temp= data[i]
        temp["label"] = data[i][label]
        options.push(temp);
    }
    return options
}